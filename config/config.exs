use Mix.Config

config :logger, :console,
  level: :error,
  flush: true
